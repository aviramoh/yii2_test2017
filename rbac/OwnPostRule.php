<?php
//OwnPostRule
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnPostRule extends Rule
{
	public $name = 'ownPostRule'; //must be here

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) { //is loged in
			return isset($params['post']) ? $params['post']->author == $user : false;
		}
		return false;
	}
}