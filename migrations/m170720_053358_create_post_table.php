<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053358_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
			'title' => $this->String(),
			'body' => $this->text(),
			'category' => $this->Integer(),
			'author' => $this->Integer(),
			'status' => $this->Integer(),
			'created_at' => $this->Integer(),
			'updated at' => $this->Integer(),
			'created_by' => $this->Integer(),
			'updated_by' => $this->Integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
