<?php

use yii\db\Migration;

/**
 * Handles adding firstname_column_lastname to table `user`.
 */
class m170720_061702_add_firstname_column_lastname_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'firstname', $this->string());
        $this->addColumn('user', 'lastname', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'firstname');
        $this->dropColumn('user', 'lastname');
    }
}
