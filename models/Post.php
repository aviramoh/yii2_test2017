<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\models\Category;
use app\models\User;
use app\models\Status;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $category
 * @property integer $author
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['category', 'author', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(), //מאכלס בערך הנכון את השדות מטה
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	 
	public function getUserAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
	
	public function getCategoryItem()
	{
		return $this->hasOne(Category::className(), ['id' => 'category']);
	}
	
	public function getStatusItem()
	{
		return $this->hasOne(Status::className(), ['id' => 'status']);
	}
	
	public function getAuthorItem()
	{
		return $this->hasOne(User::className(), ['id' => 'id']);
	}
	
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert); //must be here
		
        if ($this->isNewRecord) 
		    $this->status = 1; //draft!
		

	    return $return;
    }
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
